use assert_cmd::Command;
use predicates::prelude::*; // import the predicates crate
use std::fs;

type TestResult = Result<(), Box<dyn std::error::Error>>;
// in the preceding code, Box indicates that the error will live inside a kind of
// pointer where the memory is dynamically allocated on the heap rather than the stack,
// and dyn indicates that the method calls on the std::error::Error trait are dynamically
// dispatched. in short, the Ok part of Test Result will only ever hold the unit type,
// and the Err part can hold anything that implements the std::error::Error trait
#[test]
fn dies_no_args() -> TestResult {
    // use ? instead of Result::unwrap to unpack an Ok value or propagate an Err
    let mut cmd = Command::cargo_bin("echor")?;
    cmd.assert() // run command with no arguments and assert that
        .failure() // it fails and prints a usage statement to STDERR
        .stderr(predicate::str::contains("USAGE"));
    Ok(())
}

fn run(args: &[&str], expected_file: &str) -> TestResult {
    let expected = fs::read_to_string(expected_file)?;
    Command::cargo_bin("echor")?
        .args(args)
        .assert()
        .success()
        .stdout(expected);
    Ok(())
}

#[test]
fn hello1() -> TestResult {
    run(&["Hello there"], "tests/expected/hello1.txt")
}

#[test]
fn hello2() -> TestResult {
    run(&["Hello",  "there"], "tests/expected/hello2.txt")
}

#[test]
fn hello1_no_newline() -> TestResult {
    run(&["Hello  there", "-n"], "tests/expected/hello1.n.txt")
}

#[test]
fn hello2_no_newline() -> TestResult {
    run(&["-n","Hello", "there"], "tests/expected/hello2.n.txt")
}
