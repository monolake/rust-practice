use clap::{App, Arg}; // import the clap::App and clap::Arg structs

fn main() {
    let matches = App::new("echor") // create new App with the name echor
        .version("0.1.0")
        .author("monolake <gitlab.com/monolake>")
        .about("echo implementation written in Rust")
        .arg(
            Arg::with_name("text") // create new Arg with the name text
                .value_name("TEXT")
                .help("Input text")
                .required(true) // which is a required positional argument
                .min_values(1) // that must appear at least once
        )
        .arg(
            Arg::with_name("omit_newline") // create new Arg with the name text
                .short("n") // which is a flag that has only the short name -n
                .help("Do not print new line")
                .takes_value(false), // and takes no value
        )
        .get_matches(); // per documentation for App::get_matches,
                        // upon a failed parse an error will be displayed to the user
                        // and the process will exit with the appropriate error code

    let text = matches.values_of_lossy("text").unwrap();
    // since we make text Arg required, this will never be None,
    // therefore Option::unwrap won't ever panic

    let omit_newline = matches.is_present("omit_newline");

    // let endline= if omit_newline { "" } else { "\n" };
    // since endline is being used only in one place, the following is more rustic way:

    print!("{}{}", text.join(" "), if omit_newline { "" } else { "\n" });
}
