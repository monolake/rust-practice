use assert_cmd::Command;
/* use std::process::Command;


#[test]
fn runs() {
let mut cmd = Command::new("ls");
    let res = cmd.output();
    assert!(res.is_ok());
} */

#[test]
fn runs() {
    let mut cmd = Command::cargo_bin("hello-world").unwrap();
    cmd.assert().success().stdout("hello world\n");
}

#[test]
fn true_ok() {
    let mut cmd = Command::cargo_bin("true").unwrap();
    cmd.assert().success();
}

#[test]
fn false_not_ok() {
    let mut cmd = Command::cargo_bin("false").unwrap();
    cmd.assert().failure();
}
